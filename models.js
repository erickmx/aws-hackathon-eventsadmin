const Sequelize = require("sequelize");

const sequelize = new Sequelize({
  host: "localhost",
  port: 3306,
  username: "root",
  password: "hola",
  dialect: "mysql",
  database: "aws_events"
});

const EventUDG = sequelize.define("event_udg", {
  title: Sequelize.STRING,
  link: Sequelize.STRING,
  pubDate: Sequelize.DATE,
  origin: Sequelize.STRING
});

module.exports = {
  sequelize,
  EventUDG
};
