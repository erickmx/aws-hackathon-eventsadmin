const fs = require("fs");
const path = require("path");

exports.readFile = fileName => {
  const route = path.resolve(__dirname, `${fileName}.json`);
  console.log("====================================");
  console.log(route);
  console.log("====================================");
  return new Promise((resolve, reject) => {
    fs.readFile(route, (err, data) => {
      if (err) {
        return reject(err);
      }
      return resolve(data);
    });
  });
};

exports.writeToFile = (fileName, data) => {
  const route = path.resolve(__dirname, fileName);
  fs.WriteStream(route, data);
};
