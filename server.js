const express = require("express");
const bodyParser = require("body-parser");
const { sequelize, EventUDG } = require("./models");
const { Op } = require("sequelize");
const moment = require("moment");

const PORT = 8090;
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/convocatorias/:center", (req, res, next) => {
  const {
    params: { center }
  } = req;

  EventUDG.findAll({
    where: {
      origin: center,
      title: {
        [Op.like]: "%noticia%"
      },
      pubDate: {
        [Op.gte]: moment().startOf("month")
      }
    }
  })
    .then(convocatorias => {
      if (!convocatorias) {
        res.send("No hay eventos vigentes");
      }
      res.send(convocatorias);
    })
    .catch(err => {
      res.send("No hay eventos vigentes");
    });
});

app.get("/convocatorias/:center/:date", (req, res, next) => {
  const {
    params: { center, date }
  } = req;

  let newDate = null;

  if (date == "hoy") {
    newDate = "today";
  } else if (date == "ayer") {
    newDate = "yesterday";
  } else if (date == "mes") {
    newDate = "month";
  } else if (date == "semana") {
    newDate = "week";
  } else {
    newDate = "week";
  }

  EventUDG.findAll({
    where: {
      origin: center,
      pubDate: {
        [Op.gte]: moment().startOf(newDate)
      }
    }
  })
    .then(convocatorias => {
      if (!convocatorias) {
        res.send("No hay eventos vigentes");
      }
      res.send(convocatorias);
    })
    .catch(err => {
      res.send("No hay eventos vigentes");
    });
});

app.get("/becas/:center", (req, res, next) => {
  const {
    params: { center }
  } = req;

  EventUDG.findAll({
    where: {
      title: {
        [Op.like]: "%Becas%"
      },
      pubDate: {
        [Op.gte]: moment().startOf("month")
      }
    }
  });
});

app.get("/becas/:center/:date", (req, res, next) => {
  const {
    params: { center, date }
  } = req;

  let newDate = null;

  if (date == "hoy") {
    newDate = "today";
  } else if (date == "ayer") {
    newDate = "yesterday";
  } else if (date == "mes") {
    newDate = "month";
  } else if (date == "semana") {
    newDate = "week";
  } else {
    newDate = "week";
  }

  EventUDG.findAll({
    where: {
      title: {
        [Op.like]: "%Becas%"
      },
      pubDate: {
        [Op.gte]: moment().startOf(newDate)
      }
    }
  })
    .then(convocatorias => {
      if (!convocatorias) {
        res.send("No hay eventos vigentes");
      }
      res.send(convocatorias);
    })
    .catch(err => {
      res.send("No hay eventos vigentes");
    });
});

app.listen(PORT, () => console.log(`Server listen on port ${PORT}`));
