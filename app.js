const Watcher = require("rss-watcher");
const { EventUDG, sequelize } = require("./models");

const feedCucei = "http://www.cucei.udg.mx/rss/noticias";
const feedUdgEvents = "http://www.udg.mx/rss/tablero";
const feedUdgActs = "http://www.udg.mx/rss/actividades";

const watcherCucei = new Watcher(feedCucei);
const watcherEvents = new Watcher(feedUdgEvents);
const watcherActs = new Watcher(feedUdgActs);

watcherCucei.set({
  feed: feedCucei,
  interval: 5
});

watcherEvents.set({
  feed: feedUdgEvents,
  interval: 5
});

watcherActs.set({
  feed: feedUdgActs,
  interval: 5
});

const onNewArticle = fileName => {
  return ({ title, link, pubDate, ...article }) => {
    const newArticle = {
      title,
      link,
      pubDate
    };

    EventUDG.build({
      ...newArticle
    });
  };
};

const onError = err => console.error;
const onStop = () => console.log("finished connection");

const startWatcher = fileName => {
  return (err, articles) => {
    if (err) {
      console.error(`error to obtain feed from ${fileName}`, err);
    }
    if (!articles) {
      const articlesCleaned = [];
      writeToFile(fileName, articlesCleaned);
      return;
    }
    const articlesCleaned = articles.map(
      ({ title, link, pubDate, ...article }) => ({
        title,
        link,
        pubDate
      })
    );
    articlesCleaned.map(artCln => {
      EventUDG.build({
        ...artCln
      });
    });
  };
};

watcherCucei.on("new article", onNewArticle("cuceiEvents"));
watcherCucei.on("error", onError);
watcherCucei.on("stop", onStop);

// el que contiene las becas
watcherEvents.on("new article", onNewArticle("UDGEvents"));
watcherEvents.on("error", onError);
watcherEvents.on("stop", onStop);

watcherActs.on("new article", onNewArticle("UDGActs"));
watcherActs.on("error", onError);
watcherActs.on("stop", onStop);

watcherCucei.run(startWatcher("cuceiEvents"));
watcherEvents.run(startWatcher("UDGEvents"));
watcherEvents.run(startWatcher("UDGActs"));
